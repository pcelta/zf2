<?php

class Host
{

    /** get hostname by cookie
     *
     * @return string
     */
    public function getHost()
    {
        // get hostname by cookie
        return $_COOKIE['host_cookie'];
    }
}
