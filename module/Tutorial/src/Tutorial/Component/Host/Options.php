<?php

class Options
{
    /**
     * @var \Tutorial\Component\Host
     */
    private $host;

    public function __construct(Host $host)
    {
        $this->host = $host;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        $options = array();
        switch($this->host->getHost()){

            case 'site.host2':
                $options['CACHE_ENABLED'] = false;
                break;

            case 'site.default':
                $options['CACHE_ENABLED'] = true;
                break;
        }
        return $options;
    }
}
