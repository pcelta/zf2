<?php

class Seya
{
    const ARMADURA_DE_PEGASUS = 'Pegasus';
    const ARMADURA_DE_SAGITARIO = 'Sagitario';


    private $armaduraAtual;

    private $cosmo;

    public function __construct()
    {
        $this->cosmo = 100;
    }

    public function invocaArmadura()
    {
        $this->armaduraAtual = $this->escolheArmadura();
    }


    /**
     * @param int $dano
     */
    public function defender($dano)
    {
        $this->cosmo -= $dano;
        if ($this->cosmo <= 0) {
            $this->despertarSetimoSentido();
        }
    }


    private function despertarSetimoSentido()
    {
        $this->cosmo = 10000;
        $this->invocaArmadura();
    }

    /**
     * @return string
     */
    private function escolheArmadura()
    {
        if ($this->cosmo <= 100) {
            return self::ARMADURA_DE_PEGASUS;
        } else {
            return self::ARMADURA_DE_SAGITARIO;
        }
    }

    public function getCosmo()
    {
        return $this->cosmo;
    }

    public function setCosmo($cosmo)
    {
        $this->cosmo = $cosmo;
    }
}
