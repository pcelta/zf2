<?php

require_once ZF2_PATH. '/module/Tutorial/src/Tutorial/Component/Host/Options.php';
require_once ZF2_PATH. '/module/Tutorial/src/Tutorial/Component/Host.php';


class OptionsTest extends PHPUnit_Framework_TestCase
{

    public function testGetOptions()
    {
        $expected = array('CACHE_ENABLED' => true);

        $mockedHost = $this->getMock('Host', array('getHost'), array(), '', false);

        $mockedHost->expects($this->once())
            ->method('getHost')
            ->will($this->returnValue('site.default'));

        $option = new Options($mockedHost);
        $result = $option->getOptions();

        $this->assertEquals($expected, $result);
    }

}
