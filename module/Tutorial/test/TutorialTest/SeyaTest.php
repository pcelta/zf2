<?php

require ZF2_PATH. '/module/Tutorial/src/Tutorial/Seya.php';

class SeyaTest extends PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider providerTestEscolheArmadura
     */
    public function testEscolheArmadura($cosmo, $expected)
    {
        $seya = new Seya();
        $seya->setCosmo($cosmo);

        $objReflection = new ReflectionClass('Seya');
        $method = $objReflection->getMethod('escolheArmadura');
        $method->setAccessible(true);
        $result = $method->invoke($seya);
        $this->assertEquals($expected, $result);
    }

    public static function providerTestEscolheArmadura()
    {
        $pegasus = 'Pegasus';
        $sagitario = 'Sagitario';

        return array(
            array(1, $pegasus),
            array(100, $pegasus),
            array(0, $pegasus),
            array(101, $sagitario),
            array(1000, $sagitario),
        );
    }
}
